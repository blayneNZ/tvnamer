# File extensions to use
EXTENSIONS = ['.mkv', '.avi', '.divx', '.mp4', '.m4v']

MAX_WIDTH = 50  # Max width of the filenames to be displayed, all file names will be padded with spaces out to this width

# Automatically alias these show names to the provided show ID
ALIAS_INIT = [
    {'name': 'archer', 'id': 110381},  # archer 2009 - > archer
    {'name': 'castle', 'id': 83462}  # castle 2009 -> castle
    ]

# Location and filename to use when using the pickle caching system
PICKLE_PATH = '~/.pytvnamer/'
PICKLE_FILE = 'cache.pickle'

# Location to move completed shows to.
TV_PATH = './tv/'
