
import re

# default format 'title.3x05.episode.mkv'
FORMAT = '%(series)s.%(season)dx%(episode)02d%(title)s'


class SpaceType:
    spaces = ' '
    dots = '.'
    underscores = '_'
    none = None

    space_list = ['.', '_', ' ']


class CaseType:
    lower = 0
    captial_first = 1


class TvFormatter:

    def __init__(self, spaces=SpaceType.dots, case=CaseType.lower, format=FORMAT):
        self.spaces = spaces
        self.case = case
        self.format = format
        self.episode_re = re.compile('episode [0-9]+')

    def format_directory(self, series):
        return self.transform(series.rename_name)

    def format_file(self, series, episode):
        return self.format % (self.build_dict(series, episode))

    def build_dict(self, series, episode):
        obj = {}
        obj['series'] = self.transform(series.rename_name)
        if not episode is None:
            obj['season'] = int(episode.season)
            obj['episode'] = int(episode.episode)
            title = self.transform(episode.name)
            if title != '':
                title = self.spaces + title
            obj['title'] = title

        return obj

    def transform(self, string):
        if string is None:
            return ''

        # ingore naming shows 'episode 1-10'
        if self.episode_re.match(string.lower()):
            return ''

        output = []
        for c in string:
            charcode = ord(c)
            if c in SpaceType.space_list:
                output.append(' ')
            elif (charcode > 96 and charcode < 123) or \
                    (charcode > 64 and charcode < 91) or \
                    (charcode > 47 and charcode < 58) or \
                    charcode == 45:  # allow hypens
                output.append(c)

        string = ''.join(output)
        string = string.strip()

        if self.case == CaseType.lower:
            string = string.lower()
        elif self.case == CaseType.captial_first:
            output = []
            for x in string.split(' '):
                if len(x) > 1:
                    word = x[0].upper() + x[1:].lower()
                elif len(x) == 1:
                    word = x[0].upper()
                else:
                    word = ''

                output.append(word)
            string = ' '.join(output)

        if self.spaces == SpaceType.none:
            return string

        string = string.replace('  ', ' ').replace(' ', self.spaces)
        return string
