
import os
import os.path

from pytvnamer.tvdb.tvdb import TvDB

from pytvnamer.tvformatter import TvFormatter
from pytvnamer.tvparser import TvParser
import tvutil


import config


import logging

LOG_FILENAME = ''
LOG_FORMAT = "%(asctime)s [%(levelname)s] %(message)s"


def get_cache():
    try:
        from pytvnamer.tvdb.database.mongo import TvDBMongoCache
        CACHE = TvDBMongoCache()
        logging.debug('Using MongoDB')
    except ImportError, e:
        print e
        from pytvnamer.tvdb.database.pickle import TvDBPickleCache

        file_path = os.path.expanduser(config.PICKLE_PATH)
        if not os.path.isdir(file_path):
            os.mkdir(file_path)

        CACHE = TvDBPickleCache(os.path.join(file_path, config.PICKLE_FILE))
        logging.debug('Using Pickle from "%s"' % file_path)

    return CACHE


logging.basicConfig(filename=LOG_FILENAME, format=LOG_FORMAT, level=logging.DEBUG)


class TvNamer:
    series_choice = None
    rename = None

    def __init__(self, formats=config.EXTENSIONS, pretend=True):

        self.formats = {}
        for f in formats:
            self.formats[f] = True

        self.pretend = pretend

        self.tv = TvDB(get_cache())
        self.formatter = TvFormatter()

    def parse_directory(self, directory, recrusive=True):
        self.rename = []
        self.all_files = []
        parser = TvParser()

        files = os.listdir(directory)

        for f in files:

            if os.path.isdir(f):
                # dont go into sub dirs
                if not recrusive:
                    continue

                for x in os.listdir(f):
                    files.append(os.path.join(f, x))

                continue

            # check file exension
            ext = os.path.splitext(f)[1].lower()
            if not ext in self.formats:
                continue

            show = parser.get_show(f)
            if show is None or show.series_name is None:
                logging.error('[FILE] Unable to parse %s' % f)
                continue

            series = self.tv.find_series(show.series_name)
            if series is None:
                logging.error('[SERIES] cannot find a match for %s' % show.series_name)
                continue

            episode = self.tv.get_episode(series.id, show.season, show.episode)
            if episode is None:
                logging.error('[EPISODE] cannot find episode for show "%s"' % show)
                continue

            obj = TvRename(os.path.split(f)[0],
                            os.path.basename(f),
                            self.formatter.format_file(series, episode) + ext,
                            show,
                            series,
                            episode)

            self.all_files.append(obj)
            if not obj.should_rename():
                continue

            self.rename.append(obj)

    def rename_files(self, pretend=True):
        self.rename.sort(key=lambda x: x.get_renamepath())
        for x in self.rename:

            filepath = x.get_filepath()
            renamepath = x.get_renamepath()

            logging.info('[RENAME] %s -> %s' % (tvutil.pad(filepath, config.MAX_WIDTH), renamepath))

            if os.path.isfile(renamepath):
                logging.error('[RENAME] cant rename "%s" as file already exists' % renamepath)
                continue

            if not os.path.isfile(filepath):
                logging.error('[RENAME] cant rename "%s" as source file "%s" has been removed' % (renamepath, filepath))
                continue
            if not pretend:
                os.rename(filepath, renamepath)

    def move_files(self, pretend=True):
        for x in self.all_files:

            tv_dir = self.formatter.format_directory(x.series)
            dirname = os.path.join(config.TV_PATH, tv_dir)

            if not os.path.isdir(dirname):
                os.makedirs(dirname)

            movefile = os.path.join(dirname, x.rename)

            if os.path.isfile(movefile):
                continue
            if not os.path.isfile(x.get_renamepath()):
                logging.error('cant find file %s' % x.rename)

            logging.info('[MOVE] %s -> %s' % (tvutil.pad(x.rename, config.MAX_WIDTH), movefile))
            if pretend:
                continue

            os.rename(x.get_renamepath(), movefile)

class TvRename:

    def __init__(self, path, filename, rename, show, series, episode):
        self.path = path
        self.filename = filename
        self.rename = rename
        self.show = show
        self.series = series
        self.episode = episode

    def should_rename(self):
        return self.get_filepath() != self.get_renamepath()

    def get_filepath(self):
        return os.path.join(self.path, self.filename)

    def get_renamepath(self):
        return os.path.join(self.path, self.rename)

if __name__ == '__main__':
    tv = TvNamer()
    tv.parse_directory('../tests/files')
    tv.rename_files()
