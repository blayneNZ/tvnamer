

def strip_name(name):
    output = []
    name = name.lower()
    for x in name:
        charcode = ord(x)
        if (charcode > 122 or charcode < 97) and \
            (charcode < 48 or charcode > 58):
            continue

        output.append(x)

    return ''.join(output)


def pad(string, padding):
    length = len(string)

    if length > padding:
        return string[:padding - 3] + '...'

    return string + ' ' * (padding - length)
