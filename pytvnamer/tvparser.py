import re
import os
import logging


class ExtensionException(Exception):
    pass


class ParseException(Exception):
    pass


class TvParser:
    formats = [
        {'re':'([a-zA-Z\-\.0-9]+)[Ss]([0-9]+)[Ee]([0-9]+).*',  'series': 0, 'season':1, 'episode':2, 'format':'Show.name.S01E02.blah.mkv'},
        {'re':'([a-zA-Z\-\w\_0-9 \'\(\)]+) - ([0-9][0-9])[xX]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'show - 10x02 - blah.mkv'},
        {'re':'([a-zA-Z\-\w\_0-9 \'\(\)]+) - ([0-9]+)[xX]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'show - 1x02 - blah.mkv'},
        {'re':'([a-zA-Z\-\w 0-9]+) - ([0-9]+)[xX]([0-9]+)', 'series': 0, 'season':1, 'episode':2, 'format':'Show Name - 1x02.mkv'},
        {'re':'([a-zA-Z\-\w 0-9]+) - Season ([0-9]+) .* Episode ([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'Show Name - Season 1, Episode2 - Blah.mkv'},
        {'re':'([a-zA-Z\-\w\_\.]+).([0-9][0-9])[xX]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'show.1x02 blah.mkv'},
        {'re':'([a-zA-Z\-\w\_\.]+).([0-9]+)[xX]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'show.1x02 blah.mkv'},
        {'re':'([a-zA-Z\-\w 0-9]+) - [Ss]([0-9]+ )[Ee]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'Show Name - S01E02 blah.mkv'},
        {'re':'([a-zA-Z\-\w 0-9]+) ([0-9])([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'Show Name 102 blah.mkv'},
        {'re':'([a-zA-Z\-\.0-9 ]+)[Ss]([0-9]+)[Ee]([0-9]+).*',  'series': 0, 'season':1, 'episode':2, 'format':'Show name S01E02 blah.mkv'},
        {'re':'([a-zA-Z\-\.0-9 ]+)/[Ss]([0-9]+)[Ee]([0-9]+).*', 'series': 0, 'season':1, 'episode':2, 'format':'Show.Name/S01E01.mkv'}
    ]

    def __init__(self):
        # prepare the regexp's
        for x in self.formats:
            x['cmp'] = re.compile(x['re'])

    def strip_name(self, name):
        nx = name.lower()
        out = []
        for x in nx:
            if x in self.replace:
                out.append(x)

        return ''.join(out)

    def get_show(self, filename):

        f = os.path.splitext(filename)
        name = os.path.basename(f[0])

        # look for the filename first
        info = self.find_match(name)
        # if that doesnt work check using the directory as well
        if info is None:
            dirname = os.path.split(os.path.dirname(filename))[1]
            info = self.find_match(os.path.join(dirname, name))

        if info is None:
            return None

        info.filename = filename
        return info

    def find_match(self, filename):

        for x in self.formats:
            res = x['cmp'].match(filename)
            if res != None:
                groups = res.groups()
                try:
                    info = TvParse()
                    info.episode = int(int(groups[x['episode']]))
                    info.season = int(groups[x['season']])

                    info.match_id = x['format']

                    info.series_name = groups[x['series']].replace(".", " ").replace("_", " ").rstrip()
                    return info
                except ValueError, e:
                    logging.error(e)

        return None


class TvParse:

    filename = ''

    extension = ''

    episode = ''
    season = ''

    rename = ''

    series_name = ''
    search_name = ''  # precomputed Searching name

    def __init__(self):
        self.series_name = None
        self.season = 0
        self.episode = 0
        self.extension = None
        self.filename = None

    def format(self, output):
        return output % self

    def __str__(self):
        return "[name:%(series_name)s] [season:%(season)sx%(episode)s] from [%(filename)s]" % self.__dict__
