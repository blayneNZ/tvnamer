#!/usr/bin/env python
import httplib
import urllib
import logging
import sys

import zipfile

from xml.etree import ElementTree as ET
import time
import io


from pytvnamer.tvdb.model import TvDBMirror, TvSeries, TvShow


class TvDB:
    MIRROR_XML = 1
    MIRROR_BANNER = 2
    MIRROR_ZIP = 4

    api_key = '/api/20D8BF1C38ADDAF7'
    host = 'www.thetvdb.com'

    mirrors = None
    conn = None
    cache = None

    def __init__(self, cache=None):
        self.cache = cache
        if self.cache is None:
            logging.error('No database cache  supplied to tvdb')

        self.load_mirrors()
        self.__get_mirror(self.MIRROR_XML)
        self.search_list = []

    def __connect(self):
        if(self.conn == None):
            try:
                self.conn = httplib.HTTPConnection(self.host, 80)
            except Exception, e:
                logging.error('Failed to connect to %s %s' % (self.host, e))
                sys.exit()

    def __disconnect(self):
        if self.conn == None:
            return

        self.conn.close()
        self.conn = None

    def __get_data(self, url, keyreq=True):
        self.__connect()
        if keyreq:
            url = self.api_key + url

        logging.debug("GET %s%s" % (self.host, url))
        self.conn.request("GET", url)

        resp = self.conn.getresponse()
        if resp.status != 200:
            logging.error("Failed to fetch %s%s" % (self.host, url))
            return None
        return resp.read()

    def get_episode(self, id, season, ep, allow_updates=True):
        episode = self.cache.get_episode(id, season, ep)

        if episode is None or episode.name is None or episode.name.lower() == 'tba':

            # Allow updating of the series
            if not allow_updates:
                return None

            series = self.cache.get_series(id)

            if series.last_update is None:
                last_update = 99999999
            else:
                last_update = time.time() - series.last_update

            if last_update > 86400:
                self.update_series(id)
                # Try and refind the series
                return self.get_episode(id, season, ep, False)

            logging.debug('series "%s" [id:%s] is up to date (last update %d seconds ago) but cannot find season:%d episode:%d' %
                                (series.name, series.id, last_update, season, ep))

            return None

        return episode

    def add_alias(self, name, id):
        self.cache.add_alias(name, id)

    def find_series(self, name):
        series = []

        series = self.cache.find_series(name)

        if series is None and not name in self.search_list:
            # we only look 1 per time we are initalized
            self.search_list.append(name)
            # Look online for new series
            self.__get_mirror(self.MIRROR_XML)
            data = self.__get_data('/api/GetSeries.php?%s' % urllib.urlencode({'seriesname': name.lower()}), False)

            data = ET.XML(data)
            s = []
            for series in data:
                for tags in series:
                    if tags.tag == 'seriesid':
                        s.append(int(tags.text))

            data = []
            for id in s:
                data.append(self.get_series(id))

            if len(s) == 1:
                return self.cache.find_series(name)

            return None
        return series

    def get_series(self, id):
        s = self.cache.get_series(id)

        # TODO check for updates if lastupdate is out of date
        if s != None:
            return s

        return self.update_series(id)

    def update_series(self, id):

        self.__get_mirror(self.MIRROR_ZIP)
        data = self.__get_data('/series/%d/all/en.zip' % id)

        zf = zipfile.ZipFile(io.BytesIO(data))
        eplist = zf.read('en.xml')

        data = ET.XML(eplist)

        series = data.find('Series')
        tvseries = TvSeries(series.find('id').text, series.find('IMDB_ID').text, series.find('SeriesName').text, int(time.time()))
        self.cache.add_series(tvseries)

        eplist = []
        for x in data.findall('Episode'):
            episode = TvShow(x.find('id').text,
                            x.find('seriesid').text,
                            x.find('SeasonNumber').text,
                            x.find('EpisodeNumber').text,
                            x.find('EpisodeName').text)

            eplist.append(episode)

        self.cache.add_all_episodes(tvseries, eplist)

        return tvseries

    def __get_mirror(self, mirror_type):
        self.__disconnect()
        for x in self.mirrors:
            # found a suitable mirror
            if (x.mask & mirror_type) == mirror_type:
                # if its different from our current mirror
                # discconect from the current one
                if x.url != self.host:
                    self.host = x.url.replace('http://', '')
                return

    def load_mirrors(self):
        mirrors = self.cache.get_mirrors()
        if mirrors == None or len(mirrors) == 0:
            mirrors = self.__get_data("/mirrors.xml")
        else:
            self.mirrors = mirrors
            return

        self.mirrors = []
        data = ET.XML(mirrors)
        for m in data.findall('Mirror'):
            mirror = TvDBMirror(m.find('id').text, m.find('typemask').text, m.find('mirrorpath').text, int(time.time()))
            self.mirrors.append(mirror)

        self.cache.add_mirrors(self.mirrors)
        self.mirrors = self.cache.get_mirrors()
