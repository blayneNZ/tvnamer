import pytvnamer.tvutil as tvutil


class TvSeries:

    id = 0
    imdb = 0
    name = ''
    search_name = ''
    last_update = 0
    rename_name = ''

    def __init__(self, id, imdb, name, last_update, rename_name='', search_name='', _id=None):
        self.id = int(id)
        self.imdb = imdb
        self.name = name
        self.last_update = int(last_update)

        self.search_name = search_name
        if self.search_name == '':
            self.search_name = tvutil.strip_name(name)

        self.rename_name = rename_name
        if self.rename_name == '':
            self.rename_name = self.name

    def __str__(self):
        return "[series:%(id)s] [name:%(name)s -> %(rename_name)s] " % self.__dict__


class TvShow:

    id = 0

    series_id = 0
    season = 0
    episode = 0
    name = ''

    def __init__(self, id, series_id, season, episode, name, _id=None):
        self.id = int(id)
        self.series_id = int(series_id)
        self.season = int(season)
        self.episode = int(episode)
        self.name = name

    def format(self, output):
        return output % self

    def __str__(self):
        return "[series:%(series_id)s] [season:%(season)sx%(episode)s] " % self.__dict__


class TvDBMirror():

    id = 0
    mask = 0
    url = ''
    last_update = 0

    def __init__(self, id, mask, url, last_update, _id=None):
        self.mask = int(mask)
        self.id = int(id)
        self.url = url
        self.last_update = int(last_update)


class TvAlias():

    id = 0
    name = ''
    series_id = 0

    def __init__(self, name, id, _id=None):
        self.name = name
        self.id = int(id)
