
import logging
import pymongo

from pytvnamer.tvdb.model import TvSeries, TvShow, TvDBMirror, TvAlias

import pytvnamer.tvutil as tvutil


class TvDBMongoCache:

    def __init__(self):
        self.c = pymongo.Connection()
        self.db = self.c.pytvnamer

    def list_series(self, name):
        output = []
        if name is None or name == '':
            series = self.db.series.find().sort('search_name')

        else:
            search_name = tvutil.strip_name(name)
            series = self.db.series.find({'search_name': {'$regex': '^%s' % search_name}}).sort('search_name')

        for x in series:
            logging.info('%s' % x)
            output.append(TvSeries(**x))

        return output

    def get_episodes(self, series):
        episodes = self.db.episode.find({'series_id': series})
        output = []

        for e in episodes:
            output.append(TvShow(**e))

        return output

    def get_episode(self, series, season, ep):
        episode = self.db.episode.find_one({'series_id': series, 'season': season, 'episode': ep})
        if episode is None:
            return None
        return TvShow(**episode)

    def get_alias(self, series_id):
        alias = self.db.alias.find({'id': series_id})
        if alias is None:
            return

        output = []
        for a in alias:
            output.append(TvAlias(**a))
        return output

    def remove_all_alias(self, id):
        self.db.alias.remove({'id': id})

    def add_alias(self, name, id):
        self.db.alias.insert({'name': name, 'id': int(id)})

    def find_alias(self, name):
        alias = self.db.alias.find_one({'name': name})
        if alias is None:
            return None

        return TvAlias(**alias)

    def find_series(self, name):
        name = tvutil.strip_name(name)
        # check to see if the name is a defined alias of a show
        alias = self.find_alias(name)

        if not alias is None:
            series = self.get_series(alias.id)
            if series is None:
                return None
            return series

        series = self.db.series.find_one({'search_name': name})

        if series is None:
            return None

        return TvSeries(**series)

    def add_episode(self, ep):
        old = self.get_episode(ep.series_id, ep.season, ep.episode)

        if old != None:
            self.db.episode.remove({id: old.id})

        self.db.episode.insert(ep.__dict__)

    def update_series(self, series):
        logging.info('updating series : %s' % series)

        self.db.series.remove({'id': series.id})
        self.db.series.insert(series.__dict__)
        return True

    def get_series(self, id):
        series = self.db.series.find_one({'id': id})
        if series is None:
            return None
        return TvSeries(**series)

    def add_series(self, series):
        last = self.get_series(series.id)
        if last != None:
            self.db.series.update({'id': series.id}, series.__dict__)
            return
        self.db.series.insert(series.__dict__)

    def delete_episode(self, id):
        self.db.episode.remove({'id': id})

    def add_all_episodes(self, series, episodes):
        for x in episodes:
            old = self.get_episode(x.series_id, x.season, x.episode)

            if old != None:
                self.delete_episode(old.id)
            else:
                logging.info('New Episode [%s] [%sx%s - %s]' % (series.name, x.season, x.episode, x.name))

            self.db.episode.insert(x.__dict__)

    def get_mirrors(self):
        mirrors = self.db.mirrors.find()
        output = []
        for m in mirrors:
            output.append(TvDBMirror(**m))

        return output

    def add_mirrors(self, mirrors):
        print mirrors

        self.db.mirrors.remove()

        for m in mirrors:
            self.db.mirrors.insert(m.__dict__)
