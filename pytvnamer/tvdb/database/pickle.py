
import logging
try:
    import cPickle as pickle
except ImportError, e:
    logging.warn('Unable to load cPickle, falling back to pickle.')
    import pickle

import os
from pytvnamer.tvdb.model import TvAlias

import pytvnamer.tvutil as tvutil


class TvDBPickleCache:

    def __init__(self, path):
        self.path = path
        if not os.path.isfile(path):
            self.__init_pickle()

        self.__load()

    def __init_pickle(self):
        self.db = {'mirrors': [],
                    'series': {},
                    'episode': {},
                    'alias': {},
                    'series_name': {}}
        self.__save()

    def __save(self):
        pickle_file = open(self.path, 'wb')
        pickle.dump(self.db, pickle_file, -1)
        pickle_file.close()

    def __load(self):
        try:
            pickle_file = open(self.path, 'rb')
            self.db = pickle.load(pickle_file)
            pickle_file.close()
        except:
            logging.error('error reading pickle file : %s' % self.path)
            self.__init_pickle()

    def list_series(self, name):
        output = []
        search_name = None
        if not name is None and name != '':
            search_name = tvutil.strip_name(name)

        for x in self.db['series']:
            if search_name is None:
                output.append(self.db['series'][x])
            else:
                logging.info('searching on %s' % name)
                y = self.db['series'][x]

                if y.name.find(search_name) > 0:
                    output.append(y)

        output.sort(key=lambda x: x.name)
        return output

    def get_episodes(self, series):
        return self.db['episode'].get(series, [])

    def delete_episode(self, series, season, episode):
        episodes = self.db['episode'].get(series, [])

        self.db['episode'][series] = [x for x in episodes
                                    if x.season != season
                                    and x.episode != episode]
        self.__save()

    def get_episode(self, series, season, episode):
        episodes = self.db['episode'].get(series, [])

        for ep in episodes:
            if ep.season == season and ep.episode == episode:
                return ep

        return None

    def get_alias(self, series_id):
        output = []
        for x in self.db['alias']:
            y = self.db['alias'][x]
            if y.id == series_id:
                output.append(y)

        return output

    def add_alias(self, name, id):
        self.db['alias'][tvutil.strip_name(name)] = TvAlias(name, id)
        self.__save()

    def find_alias(self, name):
        return self.db['alias'].get(name, None)

    def find_series(self, name):
        name = tvutil.strip_name(name)
        # check to see if the name is a defined alias of a show
        alias = self.find_alias(name)

        if not alias is None:
            series = self.get_series(alias.id)
            return series

        id = self.db['series_name'].get(name, None)
        if id is None:
            return None

        return self.get_series(id)

    def add_episode(self, ep):
        old = self.get_episode(ep.series_id, ep.season, ep.episode)

        if old != None:
            self.delete_episode(ep.series_id, ep.season, ep.episode)

        data = self.db['episode'].get(ep.series_id, None)
        if data == None:
            data = []
            self.db['episode'][ep.series_id] = data

        data.append(ep)
        self.__save()

    def update_series(self, series):
        logging.info('updating series : %s' % series)

        self.db['series'][series.id] = series
        self.__save()
        return True

    def get_series(self, id):
        return self.db['series'].get(id, None)

    def add_series(self, series):
        self.db['series'][series.id] = series
        self.db['series_name'][series.search_name] = series.id

        self.__save()

    def add_all_episodes(self, series, episodes):
        for x in episodes:
            old = self.get_episode(x.series_id, x.season, x.episode)

            if old != None:
                self.delete_episode(x.series_id, x.season, x.episode)
            else:
                logging.info('New Episode [%s] [%sx%02d]  [%s]' % (series.name,
                                                x.season, int(x.episode), x.name))

            data = self.db['episode'].get(x.series_id, None)
            if data == None:
                data = []
                self.db['episode'][x.series_id] = data

            data.append(x)
        self.__save()

    def remove_all_alias(self, id):
        alias = self.db['alias']
        todelete = []
        for x in alias:

            if alias[x].id == id:
                todelete.append(x)

        for to in todelete:
            del alias[to]

        self.__save()

    def get_mirrors(self):
        return self.db['mirrors']

    def add_mirrors(self, mirrors):

        self.db['mirrors'] = []

        for m in mirrors:
            self.db['mirrors'].append(m)

        self.__save()
